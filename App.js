import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react'
import { StyleSheet, Button } from 'react-native';
import {createAppContainer,createSwitchNavigator} from 'react-navigation'; 
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome5';

import WelcomeScreen from './screens/WelcomeScreen';
import HomeScreen from './screens/HomeScreen';
import ProfileScreen from './screens/ProfileScreen';
import SignUpScreen from './screens/SignUpScreen';
import SignInScreen from './screens/SignInScreen';
import CustomDrawerContent from './DrawerContent/CustomDrawerContent';
import SettingsScreen from './screens/SettingsScreen';
import EditProfileScreen from './screens/EditProfileScreen';
import NotificationScreen from './screens/NotificationScreen';
import ChildrenInformationScreen from './screens/ChildrenInformationScreen';
import PaymentsScreen from './screens/PaymentsScreen';
import ContactUsScreen from './screens/ContactUsScreen';
import TrackDetailsScreen from './screens/TrackDetailsScreen'
import {ActionSheetProvider} from '@expo/react-native-action-sheet'



class App extends Component {
  render() {
    return (
    <ActionSheetProvider>
    <AppContainer/>
    </ActionSheetProvider>
    );
  }
}


const LoginStackNavigator = createStackNavigator({
  WelcomeScreen: {
    screen: WelcomeScreen,
    navigationOptions: {
      header: null,
      headerBackTitle: null,
    }
  },
  SignUpScreen: {
    screen: SignUpScreen,
    navigationOptions: {
      header: null,
      headerBackTitle: null,
    }
  },
  SignInScreen: {
    screen: SignInScreen,
    navigationOptions: {
      header: null,
      headerBackTitle: null,
    }
  }
})


const AppDrawerNavigator = createDrawerNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
      drawerIcon: () => <Icon name="home"size={24}/>,
      headerLeft: () => 
        <Icons.Button name="ios-menu" size={25}
        backgroundColor="#fff" color="#000" onPress={() => navigation.openDrawer()}>
        </Icons.Button>
    },
  },
  ProfileScreen: {
    screen: ProfileScreen,
    navigationOptions: {
      title: 'Profile',
      drawerIcon: () => <Icon name="account"size={24}/>
    }
  },
  ChildrenInformationScreen: {
    screen: ChildrenInformationScreen,
    navigationOptions: {
      title: 'Children Information',
      drawerIcon: () => <Icon2 name="child"size={24}/>
    }
  },
  PaymentsScreen: {
    screen: PaymentsScreen,
    navigationOptions: {
      title: 'Payments',
      drawerIcon: () => <Icon2 name="credit-card"size={24}/>
    }
  },
  ContactUsScreen: {
    screen: ContactUsScreen,
    navigationOptions: {
      title: 'Contact Us',
      drawerIcon: () => <Icon2 name="phone"size={24}/>
    }
  },
  NotificationScreen: {
    screen: NotificationScreen,
    navigationOptions: {
      title: 'Notifications',
      drawerIcon: () => <Icon name="bell" size={24}/>
    }
  },
  SettingsScreen: {
    screen: SettingsScreen,
    navigationOptions: {
      title: 'Settings',
      drawerIcon: () => <Icon name="exit-to-app" size={24}/>
    }
  },
}, {
  contentComponent: CustomDrawerContent
}
)

const AppSwitchNavigator = createSwitchNavigator({
  LoginStackNavigator,
  AppDrawerNavigator,
  EditProfileScreen: {
    screen: EditProfileScreen,
    navigationOptions: {
      headerLeft: <Button onPress={() => this.props.navigation.goBack('ProfileScreen')} />
    }
  },
  TrackDetailsScreen: {
    screen: TrackDetailsScreen,
    navigationOptions: {
      headerLeft: <Button onPress={() => this.props.navigation.goBack('HomeScreen')} />
    }
  }
});


const AppContainer = createAppContainer(AppSwitchNavigator);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


export default App;

