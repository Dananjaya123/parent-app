import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'


export const openImageLibrary = async () => {
    const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
        alert('Sorry, we need camera roll permission to select an image')

    } else {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [1,1],
            base64: true,
        })

        return !result.cancelled ? result : false;
    }
}