import React, { Component } from "react";
import { View, StyleSheet, ActivityIndicator, ScrollView, SafeAreaView} from "react-native";
import {
  Avatar,
  Title,
  Caption,
} from "react-native-paper";

import { DrawerContentScrollView, DrawerItems } from "react-navigation-drawer";
import {firebase} from '../database/firebaseDb';


export default class CustomDrawerContent extends Component {
  constructor() {
    super();
    let user = firebase.auth().currentUser
    this.firestoreRef = firebase.firestore().collection('parents').where("id", "==", user.uid);
    this.state = {
      isLoading: true,
      userArr: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const {image, userName, email} = res.data();
      userArr.push({
        key: res.id,
        res,
        image,
        userName,
        email
      });
    });
    this.setState({
      userArr,
      isLoading: false,
    });
  }

  render() {

    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }

    return (
      <ScrollView>
        <SafeAreaView/>
        {this.state.userArr.map((item, i) => {
        return (
        <View
          style={{
            height: 150,
            alignItems: "center",
            justifyContent: "center",
            marginTop: 20,
          }}
        >
          <Avatar.Image
            source={{
              uri: item.image
            }}
            size={80}
          />
          <View style={{ marginLeft: 15, flexDirection: "column" }}>
            <Title style={styles.title}>{item.userName}</Title>
            <Caption style={styles.caption}>{item.email}</Caption>
          </View>
        </View>
        )
      })}
        <DrawerItems {...this.props} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
      alignSelf: 'center',
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});
