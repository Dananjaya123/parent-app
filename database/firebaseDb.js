import * as firebase from 'firebase';
import 'firebase/auth'
import 'firebase/firestore'


const firebaseConfig = {
    apiKey: "AIzaSyCyaec4kPUdnZxMAVzaIrwvQj2htkZvkcU",
    authDomain: "school-van-tracking-syst-b0111.firebaseapp.com",
    databaseURL: "https://school-van-tracking-syst-b0111.firebaseio.com",
    projectId: "school-van-tracking-syst-b0111",
    storageBucket: "school-van-tracking-syst-b0111.appspot.com",
    messagingSenderId: "431765042746",
    appId: "1:431765042746:web:8166010c5e55bfd6cf6703",
    measurementId: "G-R577CM0H7T"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}


export {firebase};