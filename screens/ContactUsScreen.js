import React, { Component } from 'react'
import {View, StyleSheet, SafeAreaView, TextInput} from 'react-native';
import {Title, Caption, Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

export default class ContactUsScreen extends Component {
    render() {
        return (
            <KeyboardAwareScrollView style={styles.container}>
            <SafeAreaView style={styles.container}>
                <View style={styles.userInfoSection}>
                </View>
                <View style={styles.userInfoSection}>
                <View style={styles.row}>
                        <Icon name="phone" color='#777777' size={20}/>
                        <Text style={{color:'#777777', marginLeft: 20}}>+94114567990</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon name="email" color='#777777' size={20}/>
                        <Text style={{color:'#777777', marginLeft: 20}}>schoolvantracking123@gmail.com</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon name="map-marker-radius" color='#777777' size={20}/>
                        <Text style={{color:'#777777', marginLeft: 20}}>Colombo, Sri Lanka</Text>
                    </View>
                
                
                </View>
                <View style={styles.infoBoxWrapper}>
                    <View style={styles.infoBox}>
                        <Title>OR</Title>
                        <Caption>Write Us Directly</Caption>
                    </View>
                </View>
                <View style={styles.menuWrapper}>
                <View style={styles.action}>
                        <FontAwesome name="info" size={20}/>
                        <TextInput
                        placeholder="Subject"
                        placeholderTextColor="#666666"
                        autoCorrect={false}
                        style={styles.textInput}/>
                    </View>
                    <View style={styles.action}>
                        <FontAwesome name="envelope-open-text" size={20}/>
                        <TextInput
                        placeholder="Type your message here"
                        multiline={true}
                        placeholderTextColor="#666666"
                        autoCorrect={false}
                        style={styles.textInput}/> 
                    </View>
                </View>
            </SafeAreaView>
            </KeyboardAwareScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    userInfoSection: {
        paddingHorizontal: 30,
        marginBottom: 25,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
        fontWeight: '500',
    },
    row: {
        flexDirection: 'row',
        marginBottom: 25,
    },
    infoBoxWrapper: {
        borderBottomColor: '#dddddd',
        borderBottomWidth: 1,
        borderTopColor: '#dddddd',
        borderTopWidth: 1,
        flexDirection: 'row',
        height: 100,
    },
    infoBox: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    menuWrapper: {
        marginTop: 10,
    },
    menuItem: {
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: 30,
    },
    menuItemText: {
        color: '#777777',
        marginLeft: 20,
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 26,
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 35,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5,
    },
    textInput: {
        flex: 1,
        // marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
});
