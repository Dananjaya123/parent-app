import React from 'react'
import {View, StyleSheet, Text, SafeAreaView} from 'react-native'
import Map from '../components/Map'


const TrackDetailsScreen = () => {
    return (
        <SafeAreaView forceInset={{top: 'always'}}>
            <Text style={{fontSize:40}}>Track Details</Text>
            <Map/>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({});

export default TrackDetailsScreen
