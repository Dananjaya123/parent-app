import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator, Text, ScrollView, SafeAreaView, Platform } from "react-native";
// import {Permissions, Notifications} from 'expo'
import firebase from 'firebase'
import {Permissions} from 'expo-permissions'
import {Notifications} from 'expo-notifications'


export default class NotificationScreen extends Component {

    registerForPushNotificationsAsync = async() => {
        const {status: existingStatus} = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;

        if (existingStatus !== 'granted') {
            const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        if (finalStatus !== 'granted') {
            return;
        }

        try {
            
        let token = await Notifications.getExpoPushTokenAsync();

        firebase.firestore.CollectionReference('users/'+ this.currentUser.uid+'/push_token')
        .set(token);

        } catch (error) {
            console.log(error);
        }
    };

    async componentDidMount() {
        this.currentUser = await firebase.auth().currentUser
        await this.registerForPushNotificationsAsync();
    }
    render() {
        return (
            <Text>Notification</Text>
        )
    }
}
