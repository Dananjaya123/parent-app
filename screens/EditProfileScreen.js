import React, { Component } from "react";
import {
  View, 
  Text, 
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Platform,
  SafeAreaView,
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome5';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {firebase} from "../database/firebaseDb";

export default class EditProfileScreen extends Component {
  constructor() {
    super();
    let user = firebase.auth().currentUser
    this.dbRef = firebase.firestore().collection("parents").doc(user.uid);
    this.state = {
      mobile: "",
      nic: "",
      address: "",
      image: null,
      isLoading: false,
    };
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  };

  storeUser() {
    if (this.state.name === "") {
      alert("Fill at least your name!");
    } else {
      this.setState({
        isLoading: true,
      });
      this.dbRef
        .update({
          image: this.state.image,
          mobile: this.state.mobile,
          nic: this.state.nic,
          address: this.state.address,
        })
        .then((res) => {
          this.setState({
            image: null,
            mobile: "",
            nic: "",
            address: "",
            isLoading: false,
          });
          this.props.navigation.navigate("ProfileScreen");
        })
        .catch((err) => {
          console.error("Error found: ", err);
          this.setState({
            isLoading: false,
          });
        });
    }
  }

  componentDidMount () {
    this.getPermissionAsync ();
  }

  getPermissionAsync = async () => {
    if (Platform.OS !== 'web') {
      const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this ork!')
      }
    }
  };

  pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        base64: true,
        aspect: [1,1],
      }).then((result) => {
      if (!result.cancelled) {
        this.setState({image: result.uri});
        // const {height, width, type, uri} = result;
        // return this.uriToBlob(uri);
      }
    // }).then((blob) => {
    //   return this.uploadToFirebase(blob);
    })
    } catch (err) {
      console.log(err);
    }
  };

  // uriToBlob = (uri) => {
  //   return new Promise((resolve, reject) => {
  //     const xhr = new XMLHttpRequest();

  //     xhr.onload = function () {
  //       resolve(xhr.response);
  //     };

  //     xhr.onerror = function () {
  //       reject(new Error('uriToBlob failed'));
  //     };

  //     xhr.responseType = 'blob';

  //     xhr.open('GET', uri, true);
  //     xhr.send(null);

  //   });
  // }

  // uploadToFirebase = (blob) => {
  //   return new Promise((resolve, reject) => {
  //     var storageRef = firebase.storage().ref();

  //     storageRef.child('driver-profile/photo.jpg').put(blob, {
  //       contentType: 'image/jpeg'
  //     }).then((snapshot) => {
  //       blob.close();
  //       resolve(snapshot);
  //     }).catch((error) => {
  //       reject(error);
  //     });
  //   });
  // }

  render() {

    let {image} = this.state;

    return (
      <KeyboardAwareScrollView style={styles.container}>
        <SafeAreaView>
            <View style={{margin: 20}}>
                <View style={{alignItems: 'center'}}>
                    <TouchableOpacity onPress={this.pickImage}>
                        <View style={{
                            height: 100,
                            width: 100,
                            borderRadius: 15,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <ImageBackground
                            source={
                              this.state.image
                            }
                            style={{height: 100, width: 100}}
                            imageStyle={{borderRadius: 50}}
                            >
                                <View style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'flex-start',
                                }}>
                                    <Icon name="camera" size={35} color="grey"
                                    style={{
                                        opacity: 0.7,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderWidth: 1,
                                        borderColor: '#fff',
                                        borderRadius: 10,
                                    }}/>
                                </View>
                            </ImageBackground>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.action}>
                    <FontAwesome name="user-alt" size={20}/>
                    <TextInput
                    placeholder="NIC"
                    placeholderTextColor="#666666"
                    autoCorrect={false}
                    style={styles.textInput}
                    value={this.state.nic}
            onChangeText={(val) => this.inputValueUpdate(val, "nic")}
                    />
                </View>
                <View style={styles.action}>
                    <FontAwesome name="phone" size={20}/>
                    <TextInput
                    placeholder="Phone"
                    placeholderTextColor="#666666"
                    keyboardType="number-pad"
                    autoCorrect={false}
                    style={styles.textInput}
                    value={this.state.mobile}
            onChangeText={(val) => this.inputValueUpdate(val, "mobile")}
                    />
                </View>
                <View style={styles.action}>
                    <FontAwesome name="address-card" size={20}/>
                    <TextInput
                    placeholder="Address"
                    placeholderTextColor="#666666"
                    multiline={true}
                    autoCorrect={false}
                    style={styles.textInput}
                    value={this.state.address}
            onChangeText={(val) => this.inputValueUpdate(val, "address")}
                    />
                </View>
                {/* <View style={styles.action}>
                    <FontAwesome name="shuttle-van" size={20}/>
                    <TextInput
                    placeholder="Vehicle Number"
                    placeholderTextColor="#666666"
                    autoCorrect={false}
                    style={styles.textInput}
                    value={this.state.vehicle_number}
            onChangeText={(val) => this.inputValueUpdate(val, "vehicle_number")}
                    />
                </View> */}
                <TouchableOpacity style={styles.commandButton} onPress={() => this.storeUser()}>
                    <Text style={styles.panelButtonTitle}>Submit</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
        </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      flexDirection: 'column'
  },
  commandButton: {
      padding: 15,
      borderRadius: 10,
      backgroundColor: '#F9AA33',
      alignItems: 'center',
      marginTop: 30,
  },
  panel: {
      padding: 20,
      backgroundColor: '#FFFFFF',
      paddingTop: 20,
      // borderTopLeftRadius: 20,
      // borderTopRightRadius: 20,
      // shadowColor: '#000000',
      // shadowOffset: {width: 0, height: 0},
      // shadowRadius: 5,
      // shadowOpacity: 0.4,
  },
  header: {
      backgroundColor: '#FFFFFF',
      shadowColor: '#333333',
      shadowOffset: {width: -1, height: -1},
      shadowRadius: 2,
      shadowOpacity: 0.4,
      // elevation: 5,
      paddingTop: 20,
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20,
  },
  panelHeader: {
      alignItems: 'center',
  },
  panelHandle: {
      width: 40,
      height: 8,
      borderRadius: 4,
      backgroundColor: '#00000040',
      marginBottom: 10,
  },
  panelTitle: {
      fontSize: 27,
      height: 35,
  },
  panelSubTitle: {
      fontSize: 14,
      color: 'grey',
      height: 30,
      marginBottom: 10,
  },
  panelButton: {
      padding: 13,
      borderRadius: 10,
      backgroundColor: '#FF6347',
      alignItems: 'center',
      marginVertical: 7,
  },
  panelButtonTitle: {
      fontSize: 17,
      fontWeight: 'bold',
      color: '#000000',
  },
  action: {
      flexDirection: 'row',
      marginTop: 20,
      marginBottom: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#f2f2f2',
      paddingBottom: 5,
  },
  actionError: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#FF0000',
      paddingBottom: 5,
  },
  textInput: {
      flex: 1,
      // marginTop: Platform.OS === 'ios' ? 0 : -12,
      paddingLeft: 10,
      color: '#05375a',
  },
});