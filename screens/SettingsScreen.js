import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import * as firebase from "firebase/app";
import "firebase/auth";

export default class SettingsScreen extends Component {
  constructor() {
    super();
    this.state = {
      uid: "",
    };
  }

  signOut = () => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.navigation.navigate("SignInScreen");
      })
      .catch((error) => this.setState({ errorMessage: error.message }));
  };
  render() {
    this.state = {
      displayName: firebase.auth().currentUser.displayName,
      uid: firebase.auth().currentUser.uid,
    };

    return (
      <TouchableOpacity style={styles.button2} onPress={() => this.signOut()}>
        <View>
          <Text style={styles.buttonText}>Log Out</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button2: {
    alignSelf: "center",
    alignItems: "center",
    marginTop: 350,
    position: "relative",
    backgroundColor: "#F9AA33",
    width: "30%",
    height: 50,
    justifyContent: "center",
    elevation: 5,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
