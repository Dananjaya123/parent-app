import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from "react-native";

import * as Animatable from "react-native-animatable";
import { StatusBar } from "expo-status-bar";

export default class WelcomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#344955" style="light" />
        <View style={styles.header}>
          <Image source={require("../assets/logo.jpg")} style={styles.logo} />
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.title}>Welcome to School Van Tracker</Text>
          <Text style={styles.text}>Sign In with an account</Text>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate("SignUpScreen")}
          >
            <View>
              <Text style={styles.buttonText}>Get Started</Text>
            </View>
          </TouchableOpacity>
        </Animatable.View>
      </View>
    );
  }
}

const { height } = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#344955",
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  footer: {
    flex: 1,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: "black",
    fontSize: 30,
    fontWeight: "bold",
  },
  text: {
    color: "black",
    marginTop: 30,
  },
  button: {
    alignItems: "center",
    marginTop: 30,
    marginHorizontal: 200,
    backgroundColor: "#F9AA33",
    width: 100,
    height: 50,
    justifyContent: "center",
    elevation: 5,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "bold",
  },
  signIn: {
    color: "#F9AA33",
    width: 150,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    flexDirection: "row",
  },
  textSign: {
    color: "white",
    fontWeight: "bold",
  },
});
