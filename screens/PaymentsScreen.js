import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default class PaymentsScreen extends Component {
  render() {
    return (
        <>
        <TouchableOpacity style={styles.button1} onPress={() => {}}>
        <View>
          <Text style={styles.buttonText}>Card</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button2} onPress={() => {}}>
        <View>
          <Text style={styles.buttonText}>Cash on Arrival</Text>
        </View>
      </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
    button1: {
        alignSelf: "center",
        alignItems: "center",
        marginTop: 300,
        position: "relative",
        backgroundColor: "#F9AA33",
        width: "30%",
        height: 50,
        justifyContent: "center",
        elevation: 5,
      },
      button2: {
        alignSelf: "center",
        alignItems: "center",
        marginTop: 40,
        position: "relative",
        backgroundColor: "#F9AA33",
        width: "30%",
        height: 50,
        justifyContent: "center",
        elevation: 5,
      },
      buttonText: {
        fontSize: 18,
        fontWeight: "bold",
        alignContent: "center",
        textAlign: "center",
      },
});
