import React from 'react'
import {Text, StyleSheet} from 'react-native'
import MapView from 'react-native-maps'

const Map = () => {
    return (
        <MapView
            style={styles.map}
            initialRegion={{
                latitude: 6.927079,
                longitude:  79.861244,  
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
            }}
        />
    )
}

const styles = StyleSheet.create({
    map: {
        height: 350,
    }
});

export default Map
    